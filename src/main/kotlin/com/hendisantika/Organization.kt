package com.hendisantika

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-playground
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/10/18
 * Time: 09.10
 * To change this template use File | Settings | File Templates.
 */
class Organization(var id: Int, var name: String) {
    var employees: MutableList<Employee> = ArrayList()
    var departments: MutableList<Department> = ArrayList()

    constructor(id: Int, name: String, employees: MutableList<Employee>, departments: MutableList<Department>) : this(id, name) {
        this.employees.addAll(employees)
        this.departments.addAll(departments)
    }

    constructor(id: Int, name: String, employees: MutableList<Employee>) : this(id, name) {
        this.employees.addAll(employees)
    }
}