package com.hendisantika

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-playground
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/10/18
 * Time: 09.07
 * To change this template use File | Settings | File Templates.
 */
class Department(var id: Int, var name: String, var organizationId: Int) {
    var employees: MutableList<Employee> = ArrayList()

    constructor(id: Int, name: String, organizationId: Int, employees: MutableList<Employee>) : this(id, name, organizationId) {
        this.employees.addAll(employees)
    }

    fun addEmployees(employees: MutableList<Employee>): Department {
        this.employees.addAll(employees)
        return this
    }

    fun addEmployee(employee: Employee): Department {
        this.employees.add(employee)
        return this
    }

}