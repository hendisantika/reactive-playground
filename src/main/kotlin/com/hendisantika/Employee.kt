package com.hendisantika

/**
 * Created by IntelliJ IDEA.
 * Project : reactive-playground
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/10/18
 * Time: 09.07
 * To change this template use File | Settings | File Templates.
 */
data class Employee(var id: Int, var name: String, var salary: Int) {
    var organizationId: Int? = null
    var departmentId: Int? = null

    constructor(id: Int, name: String, salary: Int, organizationId: Int, departmentId: Int) : this(id, name, salary) {
        this.organizationId = organizationId
        this.departmentId = departmentId
    }

    constructor(id: Int, name: String, salary: Int, organizationId: Int) : this(id, name, salary) {
        this.organizationId = organizationId
    }

    constructor(id: Int, organizationId: Int, departmentId: Int) : this(id, "", 0) {
        this.organizationId = organizationId
        this.departmentId = departmentId
    }

}